![README.png](README.png)

# Ray
Ray is a very simple playground for non-GPU accelerated raytracing and rasterization experiments.
Its aim is to be "GPU/FPGA aware", however, in that architecture and algorithm choices should be applicable to hardware
with components designed for SIMD processing. Specifically throughput, bandwidth and memory utilization should
be implicitly considered.
Currently, multiple threads (configurable) on an SSE4.2 capable processor are utilized.

## Intermediate goals:
- Accelerate ray/bounding-box and ray/triangle intersection tests (using CUDA initially)
- Add accumulation/budgeting of cast rays for over successive frames

## Features currently include:
- Generate an equivalent G-Buffer with both the raytracer and rasterizer on first pass
- Pregenerated clustering of geometry in hierarchical LOD tree and selection for rasterizer and raytracer
- Reflections, shadows, ...
- Reader for custom Blender export files
- Reader and writer for Portable Pixmap (PPM) files
- Writer for EXR files
- FXAA postprocessing effect to address rasterization artifacts (rasterizer, raytracer), 
  numeric precision loss (raytracer) and (the lack of) texture filtering

## Dependencies
GLM 0.9.9.8-1 https://github.com/g-truc/glm
OpenEXR 3.1.2-1 https://www.openexr.com/

## License
Apache 2.0

Bunny and Happy Buddha models credited to Stanford Computer Graphics Laboratory
http://graphics.stanford.edu/data/3Dscanrep/
