- Architecture:
    - Build a CMakefile
    - Cleanup usage of projection-matrix and view-matrix, implement modelview-matrix

- Bugs:
    - Span of steps in FXAA implementation would probably look a lot better if depth is considered

- Features:
    - Elabotate tests and bugfixes
    - Add noise reduction postprocessor even though FXAA is cleaning up a lot of noise.

- Performance:
    - Parallelizing cluster split is crashing
    - Improve calculation of recursion level in cluster hierarchy based on min/max/distance
    - Restructure cluster data for optimal caching
    - Provide TLAS in scene e.g. through SDF
    - Calculating triangle extents in rasterizer is slow when there are clipping triangles
    - Calculate barycentric coordinates in rasterizer only for triangles of a min size
    - When rastering a triangle in rasterizer.cc, we could break when we have already drawn parts of the triangle in a given row/column

- Cleanups:
    - Fix for loop spacing in Geometry, ...
    - Use hashmap to return instances of images, clusters and geometry
    - Make width/height uint16_t
    - Be more consistent where we pass pointers or references
    - Store stats in module structs instead of global variables
