/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include "common.h"
#include "geometry.h"
#include "ray.h"
#include "cluster_optimize.h"

#define DEBUG_STATISTICS 0
#define DEBUG 1

static const uint16_t cluster_max_size = 64;
static const uint8_t cluster_reserve_labeled_clusters = 64;
static const float cluster_distance_ratio = 0.5f;

static void print_cluster(const Cluster& c)
{
	for(uint32_t fi = 0; fi < c.faces.size(); fi++) {
		const Triangle *tr = &c.faces[fi];
		printf("CSV,%f,%f,%f\n", tr->a.x, tr->a.y, tr->a.z);
		printf("CSV,%f,%f,%f\n", tr->b.x, tr->b.y, tr->b.z);
		printf("CSV,%f,%f,%f\n", tr->c.x, tr->c.y, tr->c.z);
	}
}

static bool ray_recurse_cluster_hierarchy(const Cluster& cluster, const Ray& ray, const vec3 ray_dir_inv,
	vector<const Cluster *>& labeled_clusters, float distance)
{
	float intersection_distance;
	if(!ray_box_intersection(ray.origin, ray_dir_inv, cluster.min, cluster.max, &intersection_distance))
		return false;

	//if(intersection_distance > distance)
	//	return false;
/*
	const float ratio = length(cluster.max - cluster.min) / intersection_distance;
	//printf("Ratio: %f\n", ratio);
	if(ratio > cluster_distance_ratio) {
		labeled_clusters.push_back(&cluster);
		return true;
	}
*/

	if(cluster.children.size() == 0) {
		labeled_clusters.push_back(&cluster);
		return true;
	}

	// TODO subtract intersection_distance from distance for falloff on next run?

	for(const Cluster& c : cluster.children)
		ray_recurse_cluster_hierarchy(c, ray, ray_dir_inv, labeled_clusters, distance);

	return true;
}

class tri {
	public:
		tri() {
			count = 0;
		}

		~tri() {
			printf("%lu triangles in cluster tested\n", count);
		}

		uint64_t count;
};

bool cluster_ray_intersection(const Cluster& cluster, const Ray& ray, Triangle **triangle,
	vec3 *intersection_point, float *distance)
{
	static class tri tt;

	const vec3 ray_dir_inv = 1.0f / ray.direction;

	// TODO make allocation static
	vector<const Cluster *> labeled_clusters;
	labeled_clusters.reserve(cluster_reserve_labeled_clusters);
	ray_recurse_cluster_hierarchy(cluster, ray, ray_dir_inv, labeled_clusters, *distance);

	Triangle *sample_triangle;
	vec3 sample_intersection_point;
	float sample_distance;
	uint8_t result = false;

//	printf("Testing %lu clusters\n", labeled_clusters.size());

	for(const Cluster* c : labeled_clusters) {

		//printf("Testing %lu faces in cluster %p\n", c->faces.size(), &c);

		for(uint32_t fi = 0; fi < c->faces.size(); fi++) {
			sample_triangle = (Triangle *) &c->faces[fi];
			tt.count += 1;

			if(ray_triangle_intersection(ray, *sample_triangle, &sample_intersection_point, &sample_distance))
				if(sample_distance < *distance) {
					*triangle = sample_triangle;
					*intersection_point = sample_intersection_point;
					*distance = sample_distance;
					result = true;
				}
		}
	}

	return result;
}

static bool box_intersection_test(vec3 origin, vec3 direction, vec3 min, vec3 max, float *distance)
{
	vec3 o_to_min = min - origin;
	vec3 o_to_max = max - origin;

	if((dot(o_to_min, direction) < 0.5f) && (dot(o_to_max, direction) < 0.5f))
		return false;

	*distance = glm::min(length(o_to_min), length(o_to_max));
	return true;
}

static bool frustum_recurse_cluster_hierarchy(const Cluster& cluster, vector<const Cluster *>& labeled_clusters,
	vec3 origin, vec3 direction, vec3 min, vec3 max, uint16_t depth)
{
	++depth;

	float distance;
	if(!box_intersection_test(origin, direction, min, max, &distance))
	//const vec3 ray_dir_inv = 1.0f / direction;
	//if(!ray_box_intersection(origin, ray_dir_inv, min, max, &distance))
		return false;

	const float ratio = (distance / length(cluster.max - cluster.min)) / depth;
	printf("Ratio: %f of distance: %f\n", ratio, distance);
	if(log(ratio) > 1.0f) {
		labeled_clusters.push_back(&cluster);
		printf("Added cluster at depth: %u\n", depth);
		return true;
	}

	if(cluster.children.size() == 0) {
		labeled_clusters.push_back(&cluster);
		printf("Added cluster with no children at depth: %u\n", depth);
		return true;
	}

	for(const Cluster& c : cluster.children)
		frustum_recurse_cluster_hierarchy(c, labeled_clusters, origin, direction, min, max, ratio);

	return true;
}

bool cluster_frustum_intersection(const Cluster& cluster, vec3 camera_position, vec3 camera_direction,
	vector<const Cluster *>& clusters)
{
	frustum_recurse_cluster_hierarchy(cluster, clusters, camera_position, camera_direction,
		cluster.min, cluster.max, 0);

	//for(const Cluster& c : cluster.children)
	//	clusters.push_back(&c);
	//clusters.push_back(&cluster);

	//for(const Cluster* c : clusters)
	//	print_cluster(*c);

	#if DEBUG
	printf("%lu clusters selected\n", clusters.size());
	#endif

	return true;
}

static void minmax_cluster(Cluster& c)
{
	for(uint32_t ti = 0; ti < c.faces.size(); ti++) {
		Triangle *tr = &c.faces[ti];

		// Initialize min and max with any value from the set
		// on the first element
		if(ti == 0) {
			c.min = tr->a;
			c.max = tr->a;
		}

		for(uint8_t axis = 0; axis < 3; axis++) {
			c.min[axis] = glm::min( glm::min(c.min[axis], tr->a[axis]), glm::min(tr->b[axis], tr->c[axis]) );
			c.max[axis] = glm::max( glm::max(c.max[axis], tr->a[axis]), glm::max(tr->b[axis], tr->c[axis]) );
		}
	}

	//printf("Cluster min: %s, max: %s\n", to_string(c.min).c_str(), to_string(c.max).c_str());
}

// Based on https://stackoverflow.com/questions/25179693/how-to-check-whether-the-point-is-in-the-tetrahedron-or-not
static bool tetrahedron_point_intersection(const vec3 a, const vec3 b, const vec3 c, const vec3 d, const vec3 coord)
{
	mat3 inv_barycentric_matrix = inverse(mat3(
		b-a,
		c-a,
		d-a
	));

	vec3 w = inv_barycentric_matrix * (coord-a);
	return all( greaterThanEqual(w, vec3(0.0f)) && lessThanEqual(w, vec3(1.0f)) ) && (w.x+w.y+w.z <= 1.0f);
}

static void tetrahedron_map(const vec3 cube[8], vec3 (*tetrahedron)[6][4])
{
	static uint8_t rotate = 0;
	++rotate %= 2;

	if(rotate == 0) {
		vec3 tt[6][4] = {
			{ cube[5], cube[6], cube[7], cube[3] },
			{ cube[5], cube[6], cube[2], cube[3] },
			{ cube[5], cube[4], cube[7], cube[3] },

			{ cube[0], cube[1], cube[2], cube[4] },
			{ cube[1], cube[2], cube[5], cube[4] },
			{ cube[0], cube[2], cube[3], cube[4] }
		};

		memcpy(tetrahedron, &tt, sizeof(tt));
	}

	if(rotate == 1) {
		vec3 tt[6][4] = {
			{ cube[1], cube[5], cube[4], cube[7] },
			{ cube[1], cube[5], cube[6], cube[7] },
			{ cube[1], cube[0], cube[4], cube[7] },

			{ cube[2], cube[3], cube[6], cube[0] },
			{ cube[2], cube[6], cube[1], cube[0] },
			{ cube[3], cube[6], cube[7], cube[0] }
		};

		memcpy(tetrahedron, &tt, sizeof(tt));
	}

	printf("Generated tetrahedron map with rotation %u\n", rotate);
}

void split_cluster(Cluster& c)
{
	uint32_t cluster_faces = c.faces.size();

	printf("Cluster has %u faces\n", cluster_faces);

	// Find the clusters extents
	minmax_cluster(c);

	if(cluster_faces < cluster_max_size)
		return;

	// Map clusters mix/max to a cubes corners
	vec3 cube[8] = {
			vec3(c.max.x, c.max.y, c.max.z),
			vec3(c.max.x, c.min.y, c.max.z),
			vec3(c.min.x, c.min.y, c.max.z),
			vec3(c.min.x, c.max.y, c.max.z),

			vec3(c.max.x, c.max.y, c.min.z),
			vec3(c.max.x, c.min.y, c.min.z),
			vec3(c.min.x, c.min.y, c.min.z),
			vec3(c.min.x, c.max.y, c.min.z)
	};

	// Now form 6 tetrahedrons
	const uint8_t tcount = 6;
	vec3 tetrahedron[6][4];
	tetrahedron_map(cube, &tetrahedron);

	// Every tetrahedron relates to one child
	c.children.resize(tcount);

	// Iterate over faces and push them top-down into a tetrahedron
	// in which the faces center is contained
	for(uint32_t fi = 0; fi < cluster_faces; fi++) {
		Triangle *tr = &c.faces[fi];
		vec3 center = (tr->a + tr->b + tr->c)/3.0f;

		bool assigned = false;

		for(uint32_t ti = 0; ti < tcount; ti++) {
			vec3 *t = tetrahedron[ti];
			if(!tetrahedron_point_intersection(t[0], t[1], t[2], t[3], center))
				continue;

			//printf("Intersection of face %u and tetrahedron %u\n", fi, ti);
			c.children[ti].faces.push_back(*tr);

			assigned = true;
			break;
		}

		if(!assigned)
			printf("Failed to assign face %u with %s\n", fi, to_string(center).c_str());
	}

	// If the split had no effect give up on this cluster for now
	for(Cluster& cc : c.children) {
		if(cc.faces.size() == cluster_faces) {
			printf("Cluster could not be further split with %u faces\n", cluster_faces);
			c.children.clear();
			return;
		}
	}

	// Remove all faces from the parent
	c.faces.clear();
	c.faces.shrink_to_fit();

#ifdef DEBUG
	printf("Cluster with %u faces spawned children with %lu %lu %lu %lu %lu %lu faces\n",
		cluster_faces,
		c.children[0].faces.size(), c.children[1].faces.size(), c.children[2].faces.size(),
		c.children[3].faces.size(), c.children[4].faces.size(), c.children[5].faces.size()
	);
#endif

	// Iterate through all child clusters splitting them
	for(Cluster& cc : c.children)
		split_cluster(cc);

	// Copy back all childrens faces and triangle_attributes for simplification
	for(Cluster& cc : c.children)
		c.faces.insert(c.faces.end(), cc.faces.begin(), cc.faces.end());

	printf("Cluster now has %lu faces\n", c.faces.size());

	// Optimize the faces in this cluster
	optimize_cluster_mesh(c);
	for(uint8_t i = 3; c.faces.size() > 500 && i; i--)
		optimize_cluster_mesh(c);
}

void cluster_construct(Cluster& cluster, const Geometry& geometry)
{
	//printf("Self-Test Result: %u\n", tetrahedron_point_intersection(vec3(1.0f, 1.0f, 0.0f), vec3(1.0f,0.0f,0.0f), vec3(0.0f), vec3(1.0f,0.0f,-1.0f), vec3(0.55f,0.5f,0.0f)));
	//printf("Self-Test Result: %u\n", tetrahedron_point_intersection(vec3(0.0f, 1.0f, -1.0f), vec3(1.0f,0.0f,0.0f), vec3(0.0f), vec3(1.0f,0.0f,-1.0f), vec3(0.5f,0.5f,-1.02f)));
	//exit(0);

	printf("Constructing Clusters\n");

	// Add triangles to cluster
	for(uint32_t fi = 0; fi < geometry.face_count; fi++) {
		Triangle triangle;
		geometry_fetch_triangle(geometry, fi, &triangle);
		cluster.faces.push_back(triangle);
	}

	// Split the cluster recursively
	split_cluster(cluster);

	//print_cluster(cluster.children[0].children[0].children[0]);
	//print_cluster(cluster);
	//for(Cluster& c : cluster.children)
	//	print_cluster(c);
}

static void write_cluster(Cluster& cluster, FILE *cf)
{
	fwrite(&cluster.min, sizeof(vec3), 1, cf);
	fwrite(&cluster.max, sizeof(vec3), 1, cf);

	uint32_t faces = cluster.faces.size();
	fwrite(&faces, sizeof(uint32_t), 1, cf);
	fwrite(&cluster.faces[0], sizeof(Triangle), faces, cf);

	uint32_t children = cluster.children.size();
	fwrite(&children, sizeof(uint32_t), 1, cf);

	for(Cluster& c : cluster.children)
		write_cluster(c, cf);
}

static void read_cluster(Cluster& cluster, FILE *cf)
{
	if(!fread(&cluster.min, sizeof(vec3), 1, cf)) return;
	if(!fread(&cluster.max, sizeof(vec3), 1, cf)) return;

	uint32_t faces = 0;
	if(!fread(&faces, sizeof(uint32_t), 1, cf)) return;

	if(faces) {
		cluster.faces.resize(faces);
		if(!fread(&cluster.faces[0], sizeof(Triangle), faces, cf)) return;
	}

	uint32_t children;
	if(!fread(&children, sizeof(uint32_t), 1, cf)) return;

	if(children) {
		cluster.children.resize(children);
		for(Cluster& c : cluster.children)
			read_cluster(c, cf);
	}
}

void cluster_load(Cluster& cluster, const Geometry& geometry, const char* geometry_filename)
{
	// Replace .m with .ch
	char cluster_filename[PATH_MAX];
	memset(cluster_filename, 0x0, PATH_MAX);
	const char *filename_prefix = strrchr(geometry_filename, '.');
	strncpy(cluster_filename, geometry_filename, filename_prefix - geometry_filename);
	strcat(cluster_filename, ".ch");

	printf("Attempting to open %s\n", cluster_filename);

	if(access(cluster_filename, F_OK) != 0) {

		// Construct the cluster hierarchy
		cluster_construct(cluster, geometry);

		// Store cluster hierarchy
		printf("Writing cluster %s\n", cluster_filename);
		FILE *cf = fopen(cluster_filename, "wb+");
		write_cluster(cluster, cf);
		fclose(cf);

		return;
	}

	// Load cluster hierarchy
	printf("Reading cluster %s\n", cluster_filename);
	FILE *cf = fopen(cluster_filename, "rb+");
	read_cluster(cluster, cf);
	fclose(cf);
}

static void clear_cluster(Cluster& cluster)
{
	cluster.faces.clear();

	for(Cluster& c : cluster.children)
		clear_cluster(c);

	cluster.children.clear();
}

void cluster_free(Cluster& cluster)
{
	clear_cluster(cluster);
}
