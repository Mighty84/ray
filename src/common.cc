/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include "common.h"

void timer_start(Timer &timer)
{
	gettimeofday(&timer.tstart, NULL);
}

// Returns elapsed time in seconds
float timer_stop(Timer &timer)
{
	gettimeofday(&timer.tend, NULL);
	timersub(&timer.tend, &timer.tstart, &timer.tresult);

	unsigned long time_in_micros = 1000000 * timer.tresult.tv_sec + timer.tresult.tv_usec;
	return time_in_micros/1000000.0f;
}
