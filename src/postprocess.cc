/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include "common.h"
#include "image.h"

static inline float luminance(vec3 color)
{
	// See: https://www.w3.org/TR/AERT/#color-contrast
	return 0.299*color.x + 0.587*color.y + 0.114*color.z;
}

static inline uint32_t pixel_offset(uint32_t channels, uint32_t width, uint32_t height, uint32_t x, uint32_t y)
{
	// Clamp input
	uint16_t cx = MIN(MAX(x, 0), width-1);
	uint16_t cy = MIN(MAX(y, 0), height-1);

	return (cy*width*channels) + cx*channels;
}

void postprocess_fxaa(const Image& input_image, Image& output_image)
{
	uint16_t width = input_image.width;
	uint16_t height = input_image.height;

	uint32_t luma_buffer_size = width * height * sizeof(float);
	float *luma_buffer = (float*) malloc(luma_buffer_size);
	if(luma_buffer == NULL) {
		fprintf(stderr, "Failed to allocate memory for luminance buffer.\n");
		exit(EXIT_FAILURE);
	}

	// Calculate luminance for every pixel
	#pragma omp parallel num_threads(raytrace_threads)
	#pragma omp for
	for(uint32_t y = 0; y < height; y++)
		for(uint32_t x = 0; x < width; x++) {
			vec3 color = image_read_vec3(input_image, x, y);
			luma_buffer[pixel_offset(1, width, height, x, y)] = luminance(color);
		}

	// Loosely based on FXAA paper by Timothy Lottes
	#pragma omp parallel num_threads(raytrace_threads)
	#pragma omp for
	for(uint32_t y = 0; y < height; y++) {
		for(uint32_t x = 0; x < width; x++) {

			// Get necessary samples for kernel
			float luma_m =  luma_buffer[pixel_offset(1, width, height, x, y)];
			float luma_nw = luma_buffer[pixel_offset(1, width, height, x-1, y+1)];
			float luma_ne = luma_buffer[pixel_offset(1, width, height, x+1, y+1)];
			float luma_sw = luma_buffer[pixel_offset(1, width, height, x-1, y-1)];
			float luma_se = luma_buffer[pixel_offset(1, width, height, x+1, y-1)];

			// Extract smallest and largest sample
			float luma_min = glm::min(luma_m, glm::min(glm::min(luma_nw, luma_ne), glm::min(luma_sw, luma_se)));
			float luma_max = glm::max(luma_m, glm::max(glm::max(luma_nw, luma_ne), glm::max(luma_sw, luma_se)));

			vec3 color = image_read_vec3(input_image, x, y);

			// Eary-out if gradient range (min to max) is too small
			float luma_range = luma_max - luma_min;
			if(luma_range > 0.1f) {

				// Compute gradient direction
				vec2 dir;
				dir.x = (luma_nw + luma_ne) - (luma_sw + luma_se);
				dir.y = (luma_nw + luma_sw) - (luma_ne + luma_se);

				dir = normalize(dir);

				// Get some more color-samples across the span of the dir vector (in both directions)
				vec3 span_pos = vec3(0.0f);
				for(int i = 1; i < postprocess_fxaa_span_length; i++) {

					// Positive
					uint32_t cx = MIN(MAX(x+dir.x*i, 0), width-1);
					uint32_t cy = MIN(MAX(y+dir.y*i, 0), height-1);

					span_pos += (i/(float)postprocess_fxaa_span_length) * image_read_vec3(input_image, cx, cy);
				}

				vec3 span_neg = vec3(0.0f);
				for(int i = 1; i < postprocess_fxaa_span_length; i++) {

					// Negative
					uint32_t cx = MIN(MAX(x-dir.x*i, 0), width-1);
					uint32_t cy = MIN(MAX(y-dir.y*i, 0), height-1);

					span_neg += (i/(float)postprocess_fxaa_span_length) * image_read_vec3(input_image, cx, cy);
				}

				// Compute average
				color = (1.5f*color + 0.75f*span_pos + 0.75f*span_neg)/3.0f;
			}

			// Save the result to output buffer
			image_write_vec3(output_image, x, y, color);
		}
	}

	free(luma_buffer);
}

void postprocess_depth_buffer(const Image& input_image, Image& output_image)
{
	uint16_t width = input_image.width;
	uint16_t height = input_image.height;

	// Find the min and max values, hence the range
	float min_value = projection_near;
	float max_value = 0.0f;

	for(uint16_t y = 0; y < height; y++)
		for(uint16_t x = 0; x < width; x++) {
			float depth = image_read_vec1(input_image, x, y).x;

			if(depth == projection_far)
				continue;

			min_value = glm::max(projection_near, glm::min(min_value, depth));
			max_value = glm::max(max_value, depth);
		}

	// Map all pixels from the found range to 0.0f-1.0f
	for(uint16_t y = 0; y < height; y++)
		for(uint16_t x = 0; x < width; x++) {
			float depth = image_read_vec1(input_image, x, y).x;

			if(depth == projection_far) {
				image_write_vec3(output_image, x, y, vec3(0.0f));
				continue;
			}

			// Mapping operation
			float c = 1.0f - ((depth - min_value) / (max_value - min_value));

			// Store result
			image_write_vec3(output_image, x, y, vec3(c));
		}
}

void postprocess_tonemap_buffer(const Image& input_image, Image& output_image)
{
	uint16_t width = input_image.width;
	uint16_t height = input_image.height;

	for(uint16_t y = 0; y < height; y++)
		for(uint16_t x = 0; x < width; x++) {

			vec3 color = image_read_vec3(input_image, x, y);

			// Don't consider zero color (unset fragments)
			if((color.x + color.y + color.z) == 0.0f) {
				image_write_vec3(output_image, x, y, vec3(0.0f));
				continue;
			}

			// Reinhard tone-mapping
			vec3 mapped = color / (color + vec3(1.0f));

			// Gamma correction
			mapped = pow(mapped, vec3(1.0f / postprocess_tonemapping_gamma_correction));

			image_write_vec3(output_image, x, y, mapped);
		}
}

void postprocess_normal_buffer(const Image& input_image, Image& output_image)
{
	uint16_t width = input_image.width;
	uint16_t height = input_image.height;

	for(uint16_t y = 0; y < height; y++)
		for(uint16_t x = 0; x < width; x++) {

			vec3 normal = image_read_vec3(input_image, x, y);

			// Show zero normals (unset fragents) als black instead of gray
			if((normal.x + normal.y + normal.z) == 0.0f) {
				image_write_vec3(output_image, x, y, vec3(0.0f));
				continue;
			}

			normal = 0.5f*(normal+1.0f);

			image_write_vec3(output_image, x, y, normal);
		}
}
