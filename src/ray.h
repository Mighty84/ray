/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef RAY_H
#define RAY_H

inline bool ray_plane_intersection(const Ray& ray, const Plane &plane, vec3 *intersection_point, float *distance)
{
	// Ray direction should have been normalized beforehand
	// Plane normal to ray direction
	float pn_to_rd = dot(plane.normal, ray.direction);

	// Ray never intersects plane
	if(pn_to_rd > 0.0f) {
		//printf("Ray will never intersect the plane.\n");
		return false;
	}

	// We project the ray.origin onto the plane origin/space
	// and scale the planes normal to retrieve the (shortest) distance of the ray.origin
	// to the plane
	// The plane.normal is expected to be normalized so no need to devide term by length(plane.normal)
	// https://www.youtube.com/watch?v=HW3LYLLc60I
	float proj = -(dot(ray.origin - plane.origin, plane.normal));

	// Scale the projected distance vector with the angle of
	// the plane.normal and the ray.direction to oneanother
	float t = proj / pn_to_rd;

	// Only consider intersection from origin towards direction, NOT BACKWARDS
	if(t < 0.0f)
		return false;

	// Return distance to caller
	*distance = t;

	// Extend ray along the (normalized) ray direction to the intersection point at distance t
	*intersection_point = ray.origin + (ray.direction * t);

#if 0
printf("Ray: %s %s Plane: %s %s, Intersection Point: %s, Ray extrapolated using length t: %f\n",
	to_string(ray.origin).c_str(), to_string(ray.direction).c_str(),
	to_string(plane.origin).c_str(), to_string(plane.normal).c_str(),
	to_string(*intersection_point).c_str(), t);
#endif

	return true;
}

inline float point_to_edge_distance(const vec3& a, const vec3& b, const vec3& normal, const vec3& p)
{
	const vec3 n = cross(a - b, normal);
	return dot(p - a, n);
}

inline bool ray_triangle_intersection(const Ray& ray, const Triangle& triangle, vec3 *intersection_point, float *distance)
{
	// Get the intersection point on a plane derived from the triangle
	const Plane plane = {
		triangle.a, // We could take the center of the triangle, but actually any point on the plane will do
		triangle.flat_normal
	};

	// Calculate the ray/plane intersection point
	if(!ray_plane_intersection(ray, plane, intersection_point, distance))
		return false;

	// The intersection point with the plane must be in the confines of the triangles edges
	float da = point_to_edge_distance(triangle.a, triangle.b, plane.normal, *intersection_point);
	if(da > 0.0f) return false;

	float db = point_to_edge_distance(triangle.b, triangle.c, plane.normal, *intersection_point);
	if(db > 0.0f) return false;

	float dc = point_to_edge_distance(triangle.c, triangle.a, plane.normal, *intersection_point);
	if(dc > 0.0f) return false;

	//float f = length(cross(triangle.a-triangle.b, triangle.b-triangle.c));
	//printf("da: %f, db: %f, dc: %f\n", da*f, db*f, dc*f);

	//printf("Edge distances E_a %f, E_b %f, E_c %f\n", da, db, dc);
	//printf("Found triangle intersection point at: %s\n", to_string(plane_intersection_point).c_str());
	return true;
}

inline bool ray_box_intersection(const vec3& ray_origin, const vec3& rdir_inv, const vec3& min, const vec3& max, float *distance)
{
	// Based on https://tavianator.com/2011/ray_box.html and discussion
	// rdir_inv shall be 1.0f/ray.direction and saves an expensive division here

	const vec3 t1 = (min - ray_origin) * rdir_inv;
	const vec3 t2 = (max - ray_origin) * rdir_inv;

	vec3 vmin = glm::min(t1, t2);
	vec3 vmax = glm::max(t1, t2);

	float fmin = vmin[0];
	float fmax = vmax[0];
	fmin = glm::max(fmin, vmin[1]);
	fmax = glm::min(fmax, vmax[1]);
	fmin = glm::max(fmin, vmin[2]);
	fmax = glm::min(fmax, vmax[2]);

	*distance = fmin > 0.0f ? fmin : fmax;
	return fmax >= fmin && fmax >= 0.0f;
}

#endif
