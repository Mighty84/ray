/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include "common.h"
#include "geometry.h"
#include "triangle.h"
#include "image.h"

void material_load(Material& material, const char* diffuse_filename, const char* normal_filename, const char* emission_filename, const char* specular_filename)
{
	image_load_PPM(diffuse_filename, material.diffuse_texture);
	image_load_PPM(normal_filename, material.normal_texture);
	image_load_PPM(emission_filename, material.emission_texture);
	image_load_PPM(specular_filename, material.specular_texture);
}

void material_free(Material& material)
{
	image_free(material.diffuse_texture);
	image_free(material.normal_texture);
	image_free(material.emission_texture);
	image_free(material.specular_texture);
}

bool material_compute_fragment_attributes(const Fragment& fragment, FragmentAttributes& fragment_attributes)
{
	const Material& material = fragment.object->material;
	const Triangle& triangle = *fragment.triangle;

	// Retrieve the image space uv coordinate and look up the diffuse texture color
	const vec3 weights = fragment.weights;
	vec2 uv = triangle_interpolate<vec2>(triangle.uv1, triangle.uv2, triangle.uv3, weights);

	// Read color, emission, specular, ...
	fragment_attributes.color = image_read_vec3(material.diffuse_texture, uv);
	fragment_attributes.emission = image_read_vec3(material.emission_texture, uv);
	fragment_attributes.specular = image_read_vec3(material.specular_texture, uv);

	// Obtain normal for pixel
	if(material_smooth_shading_enable)
		fragment_attributes.normal = triangle_interpolate<vec3>(triangle.vertex_normal1, triangle.vertex_normal2, triangle.vertex_normal3, weights);
	else {
		fragment_attributes.normal = triangle.flat_normal;
	}

	if(material_normal_mapping_enable) {
		vec3 t = -triangle_interpolate<vec3>(triangle.tangent_normal1, triangle.tangent_normal2, triangle.tangent_normal3, weights);
		vec3 b = cross(fragment_attributes.normal, t);
		mat3 TBN = mat3(t, b, fragment_attributes.normal);
		vec3 tn = image_read_vec3(material.normal_texture, vec2(uv.x, 1.0f-uv.y)) * 2.0f - 1.0f;
		fragment_attributes.normal = normalize(TBN * tn);
	}

	return true;
}

uint8_t material_compute_sample_rays(const Fragment& fragment, const FragmentAttributes& fragment_attributes, Ray *rays, uint8_t step)
{
	// Move the world-space coordinate just slightly closer
	// to the camera, adding offset the the object the fragment originated from
	vec3 corrected_origin = fragment.source + ((fragment.intersection_point - fragment.source) * 0.99999f);

	vec3 incident_vector = corrected_origin - fragment.source;

	rays[0].origin = corrected_origin;
	rays[0].direction = reflect(incident_vector, fragment_attributes.normal);

//	if(step > 1)
//		return 1;

	rays[1].origin = corrected_origin;
	rays[1].direction = fragment_attributes.normal;

	rays[2].origin = corrected_origin;
//	rays[2].direction = normalize(fragment_attributes.normal * (ballRand(0.25f)+0.25f));
	rays[2].direction = fragment_attributes.normal;

	return 3;
}

bool material_compute_sample_aggregation(FragmentAttributes* sample_attributes,
	FragmentAttributes& fragment_attributes)
{
	vec3 sample_color = vec3(0.0f);
	uint8_t sample_count = 0;

	for(uint8_t si = 0; si < reflectance_max_samples; si++) {
		sample_color += sample_attributes[si].color;
		++sample_count;
	}

	if(sample_count > 0) {
		vec3 ref = (sample_color/(float)sample_count) * fragment_attributes.specular;
		ref += fragment_attributes.color * (fragment_attributes.emission + (1.0f-fragment_attributes.specular));
		fragment_attributes.color = ref/2.0f;
	}

	return true;
}
