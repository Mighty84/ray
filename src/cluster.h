/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef CLUSTER_H
#define CLUSTER_H

void cluster_load(Cluster& cluster, const Geometry& geometry, const char* geometry_filename);
void cluster_free(Cluster& cluster);
bool cluster_ray_intersection(const Cluster& cluster, const Ray& ray, Triangle **triangle, vec3 *intersection_point, float *distance);
bool cluster_frustum_intersection(const Cluster& cluster, vec3 min, vec3 max, vector<const Cluster *>& clusters);

#endif
