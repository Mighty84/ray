/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include "common.h"
#include "cluster.h"
#include "image.h"
#include "material.h"
#include "geometry.h"

Object *object_load(const char *geometry_filename, const char *diffuse_filename,
	const char *normal_filename, const char *emission_filename, const char* specular_filename)
{
	Object* object = new Object();
	material_load(object->material, diffuse_filename, normal_filename, emission_filename, specular_filename);
	geometry_load(object->geometry, geometry_filename);

	// Load/Create cluster hierarchy
	cluster_load(object->cluster, object->geometry, geometry_filename);

	return object;
}

void object_free(Object* object)
{
	material_free(object->material);
	geometry_free(object->geometry);

	delete object;
}

void scene_load(Scene& scene)
{
	scene.objects.push_back(object_load("data/happy_buddha.m", "data/checkers.ppm", "data/normal_ripple_hr.ppm", "data/no_emission.ppm", "data/high_emission.ppm"));
	//scene.objects.push_back(object_load("data/bunny.m", "data/checkers.ppm", "data/normal_ripple.ppm", "data/no_emission.ppm", "data/high_emission.ppm"));
	scene.objects.push_back(object_load("data/lamp.m", "data/spot_emission.ppm", "data/normal.ppm", "data/spot_emission.ppm", "data/clouds.ppm"));
	scene.objects.push_back(object_load("data/reflector.m", "data/red.ppm", "data/normal.ppm", "data/high_emission.ppm", "data/high_emission.ppm"));
	scene.objects.push_back(object_load("data/floor.m", "data/white.ppm", "data/normal.ppm", "data/no_emission.ppm", "data/high_emission.ppm"));
	scene.objects.push_back(object_load("data/cubes.m", "data/white.ppm", "data/normal.ppm", "data/no_emission.ppm", "data/high_emission.ppm"));
	scene.objects.push_back(object_load("data/ceiling.m", "data/white.ppm", "data/normal.ppm", "data/high_emission.ppm", "data/high_emission.ppm"));
}

void scene_free(Scene& scene)
{
	for(Object* o : scene.objects)
		object_free(o);

	scene.objects.clear();
}
