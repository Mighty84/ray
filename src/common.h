/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/param.h>

#include <omp.h>

#define GLM_MESSAGES
#define GLM_ENABLE_EXPERIMENTAL
#define GLM_FORCE_RADIANS
#include <glm/gtx/string_cast.hpp> // glm::to_string()
#include <glm/gtx/hash.hpp> // std::hash for vec3

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/ext/matrix_clip_space.hpp> // glm::perspective
#include <glm/gtx/transform.hpp> // Matrix operations
#include <glm/gtx/normal.hpp> // glm::triangleNormal
#include <glm/gtc/random.hpp> // glm::circularRand, glm::ballRand

#include <list>
#include <vector>
#include <queue>
#include <unordered_map>

using namespace glm;
using namespace std;

// Parameters to tune behavior
static const uint32_t raytrace_threads = 6;
static const bool raytrace_scene_pass = false;
static const bool material_smooth_shading_enable = true;
static const bool material_normal_mapping_enable = true;
static const bool postprocess_fxaa_enable = true;
static const uint8_t postprocess_fxaa_span_length = 3;
static const float postprocess_tonemapping_enable = false;
static const float postprocess_tonemapping_gamma_correction = 1.1f;
static const float projection_near = 0.1f;
static const float projection_far = 100.0f;
static const uint8_t reflectance_max_steps = 2;
static const uint8_t reflectance_max_samples = 3;

struct Image {
	uint32_t width;
	uint32_t height;
	uint8_t channels;
	float *buffer;
};

struct Material {
	Image diffuse_texture;
	Image normal_texture;
	Image emission_texture;
	Image specular_texture;
};

struct Triangle {
	vec3 a, b, c;
	vec3 flat_normal;

	vec3 vertex_normal1, vertex_normal2, vertex_normal3;
	vec2 uv1, uv2, uv3;
	vec3 tangent_normal1, tangent_normal2, tangent_normal3;
};

struct Cluster {
	vec3 min, max;
	vector<Triangle> faces;
	vector<Cluster> children;
};

struct Geometry {
	uint32_t frame_num;
	uint32_t frame_count;
	uint32_t face_count;
	float **vertices;
	float **normals;
	float *texture_coordinates;
	vec3 *flat_normals;
	vec3 *tangent_normals;
};

struct Plane {
	vec3 origin;
	vec3 normal;
};

struct Ray {
	vec3 origin;
	vec3 direction;
};

struct Object {
	Material material;
	Geometry geometry;
	Cluster cluster;
};

struct Fragment {
	const Object *object;
	const Triangle *triangle;
	vec3 intersection_point;
	vec3 source;
	vec3 weights;
};

struct FragmentAttributes {
	vec3 color;
	vec3 normal;
	vec3 emission;
	vec3 specular;
};

struct GBuffer {
	uint32_t width;
	uint32_t height;

	Fragment *fragments;
	FragmentAttributes *fragment_attributes;
};

struct Scene {
	list<Object*> objects;
};

struct Timer {
	struct timeval tstart, tend, tresult;
};

void timer_start(Timer &timer);
float timer_stop(Timer &timer);

#endif
