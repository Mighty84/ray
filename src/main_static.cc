/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include "common.h"
#include "scene.h"
#include "image.h"
#include "gbuffer.h"
#include "raytracer.h"
#include "rasterizer.h"
#include "postprocess.h"
#include "test.h"

int main()
{
#if 0
	// Run self-tests
	test();
	return EXIT_SUCCESS;
#endif

	const float fov = 50.0f;
	const uint32_t width = 1024;
	const uint32_t height = 768;

	const vec3 camera_position = vec3(0.5f, 0.25f, -3.0f);
	//const vec3 camera_direction = vec3(0.0f, 0.0f, -1.0f);
	//const vec3 camera_up_vector = vec3(0.0f, 1.0f, 0.0f);
	//mat4 viewMatrix = lookAt(camera_position, camera_position + camera_direction, camera_up_vector);
	//mat4 viewMatrix = lookAt(camera_position, camera_direction, camera_up_vector);

	mat4 viewMatrix = mat4(1.0f);
	viewMatrix = translate(viewMatrix, camera_position);
	viewMatrix = rotate(viewMatrix, radians(30.0f), vec3(0.0f, 1.0f, 0.0f));

	// Create the scene
	Scene scene;
	scene_load(scene);

	// Allocate gbuffer
	GBuffer gbuffer;
	gbuffer_init(gbuffer, width, height);

	// Render scene to image buffer
	Timer raytrace_timer;
	timer_start(raytrace_timer);

	if(raytrace_scene_pass)
		raytracer_render_scene(scene, viewMatrix, fov, gbuffer);
	else
		rasterizer_render_scene(scene, viewMatrix, fov, gbuffer);

	float render_time = timer_stop(raytrace_timer);
	printf("Scene rendered in %fs ~ %ffps\n", render_time, 1.0f/render_time);

	// Save scene pass gbuffer
	gbuffer_save_PPM(gbuffer, "scene");

	// Render reflectance
	GBuffer rbuffer;
	gbuffer_init(rbuffer, width, height);

	Timer raytrace_reflectance_timer;
	timer_start(raytrace_reflectance_timer);

	raytracer_render_reflectance(scene, gbuffer, rbuffer);

	float render_reflectance_time = timer_stop(raytrace_reflectance_timer);
	printf("Reflectance rendered in %fs ~ %ffps\n", render_reflectance_time, 1.0f/render_reflectance_time);

	// Now apply FXAA
	if(postprocess_fxaa_enable) {
		Image rbuffer_color_image;
		image_init(rbuffer_color_image, width, height, 3);

		gbuffer_blit_color(rbuffer, rbuffer_color_image);
		image_save_PPM(rbuffer_color_image, "reflectance.ppm");

		Image fxaa_image;
		image_init(fxaa_image, width, height, 3);

		Timer fxaa_timer;
		timer_start(fxaa_timer);

		postprocess_fxaa(rbuffer_color_image, fxaa_image);

		printf("FXAA applied in %fs\n", timer_stop(fxaa_timer));

		if(postprocess_tonemapping_enable)
			postprocess_tonemap_buffer(fxaa_image, fxaa_image);

		image_save_PPM(fxaa_image, "fxaa.ppm");
		image_free(fxaa_image);

		image_free(rbuffer_color_image);
	}

	gbuffer_free(rbuffer);

	// Discard gbuffer
	gbuffer_free(gbuffer);

	// Remove scene and associated assets
	scene_free(scene);

	return EXIT_SUCCESS;
}
