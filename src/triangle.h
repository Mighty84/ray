/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef TRIANGLE_H
#define TRIANGLE_H

template<typename T>
T triangle_interpolate(const T& ia, const T& ib, const T& ic, const vec3& weights)
{
	return ia * weights.x + ib * weights.y + ic * weights.z;
}

template<typename T>
vec3 triangle_barycentric_weights(const T& a, const T& b, const T& c, const T& coord)
{
	// Based on Christer Ericson's Real-Time Collision Detection
	T v0 = b - a;
	T v1 = c - a;
	T v2 = coord - a;

	float d00 = dot(v0, v0);
	float d01 = dot(v0, v1);
	float d11 = dot(v1, v1);
	float d20 = dot(v2, v0);
	float d21 = dot(v2, v1);

	float denom = d00 * d11 - d01 * d01;

	float v = (d11 * d20 - d01 * d21) / denom;
	float w = (d00 * d21 - d01 * d20) / denom;
	float u = 1.0f - v - w;

	return vec3(u, v, w);
}

#endif
