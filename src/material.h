/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef MATERIAL_H
#define MATERIAL_H

void material_load(Material& material, const char* diffuse_filename, const char* normal_filename, const char* emission_filename, const char* specular_filename);
void material_free(Material& material);
bool material_compute_fragment_attributes(const Fragment& fragment, FragmentAttributes& fragment_attributes);
uint8_t material_compute_sample_rays(const Fragment& fragment, const FragmentAttributes& fragment_attributes, Ray *rays, uint8_t step);
bool material_compute_sample_aggregation(FragmentAttributes* sample_attributes, FragmentAttributes& fragment_attributes);

#endif
