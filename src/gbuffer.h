/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef GBUFFER_H
#define GBUFFER_H

void gbuffer_init(GBuffer& gbuffer, uint32_t width, uint32_t height);
void gbuffer_free(GBuffer& gbuffer);
void gbuffer_blit_color(GBuffer& gbuffer, Image& output_image);
void gbuffer_save_PPM(GBuffer& gbuffer, const char* filename);

inline void gbuffer_read_fragment(const GBuffer& gbuffer, uint32_t x, uint32_t y, Fragment* fragment)
{
	uint32_t offset = y*gbuffer.width + x;
	*fragment =  gbuffer.fragments[offset];
}

inline void gbuffer_write_fragment(GBuffer& gbuffer, const Fragment& fragment, uint32_t x, uint32_t y)
{
	uint32_t offset = y*gbuffer.width + x;
	gbuffer.fragments[offset] = fragment;
}

inline void gbuffer_read_fragment_attributes(const GBuffer& gbuffer, uint32_t x, uint32_t y, FragmentAttributes *fragment_attributes)
{
	uint32_t offset = y*gbuffer.width + x;
	*fragment_attributes = gbuffer.fragment_attributes[offset];
}

inline void gbuffer_write_fragment_attributes(GBuffer& gbuffer, const FragmentAttributes& fragment_attributes, uint32_t x, uint32_t y)
{
	uint32_t offset = y*gbuffer.width + x;
	gbuffer.fragment_attributes[offset] = fragment_attributes;
}

#endif
