/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include "common.h"
#include "ray.h"
#include "image.h"

bool test_image()
{
	Image image;
	image_load_PPM("texture.ppm", image);
	vec2 uv = vec2(0.5, 0.5);
	vec3 color = image_read_vec3(image, uv);
	printf("Read color from %s: %s\n", to_string(uv).c_str(), to_string(color).c_str());
	free(image.buffer);

	// Check color

	return true;
}

bool test_plane_intersection()
{
	Ray ray = {
		vec3(0.0f, 0.0f, 0.0f),
		vec3(0.0f, 0.0f, 1.0f)
	};

	// Test plane that is offset behind the camera on the z-axis
	Plane plane = {
		vec3(0.0f, 0.0f, -10.0f),
		vec3(0.0f, 0.0f, 1.0f)
	};

	vec3 intersection_point;
	float distance;
	bool result = ray_plane_intersection(ray, plane, &intersection_point, &distance);

	// Check result

	return true;
}

/*
These results of ray_plane_intersection cannot be true but were reported to be
Ray: vec3(0.000000, 0.000000, 0.000000) vec3(-0.998724, -1.333861, 1.000000) Plane: vec3(-3.153895, -2.001347, -2.665185) vec3(0.000000, -1.000000, -0.000000), Intersection Point: vec3(1.498502, 2.001347, -1.500416)
Ray: vec3(0.000000, 0.000000, 3.000000) vec3(-0.498618, -0.496124, -1.000000) Plane: vec3(1.000000, -1.000000, -1.000000) vec3(1.000000, 0.000000, 0.000000), Intersection Point: vec3(-1.000000, -0.995000, 0.994455), Ray extrapolated using length t: 2.005545
Ray: vec3(0.000000, 0.000000, 3.000000) vec3(-0.496774, -0.499843, -1.000000) Plane: vec3(1.000000, -1.000000, 1.000000) vec3(0.000000, 0.000000, 1.000000), Intersection Point: vec3(0.993549, 0.999686, 5.000000), Ray extrapolated using length t: 2.000000
*/
bool test_ray_plane_intersection()
{
	Ray ray = {
		vec3(0.000000, 0.000000, 3.000000),
		vec3(-0.498618, -0.496124, -1.000000)
	};

	Plane plane = {
		vec3(1.000000, -1.000000, -1.000000),
		vec3(1.000000, 0.000000, 0.000000)
	};

	vec3 intersection_point;
	float distance;
	ray_plane_intersection(ray, plane, &intersection_point, &distance);

	// Was: vec3(-1.000000, -0.995000, 0.994455)
	printf("Intersection point: %s\n", to_string(intersection_point).c_str());
	
	return true;
}


/*
Crash:
Ray: vec3(0.000000, 0.000000, 0.000000) vec3(0.014246, -1.108987, 1.000000) Plane: vec3(-4.482178, 0.510271, -8.291862) vec3(-0.548363, -0.800906, -0.240515), Intersection Point: vec3(-0.090024, 7.008082, -6.319356)
f1 f2 f3 is vec3(-4.392155, -6.497723, -1.972586) vec3(-4.446338, -6.479880, -1.908469) vec3(-4.548537, -6.379836, -2.008601) vec3(-0.090023, 7.007994, -6.319276)
Calculated weights: A 0.014956 A_a 94.403351 A_b 107.671806 Ac 46.405647
UV is vec2(134.640274, 28.914274)
make: *** [Makefile:2: all] Error 139
*/
#if 0
bool test_point_to_edge_distance()
{
	Triangle triangle = {
		vec3(-4.392155f, -6.497723f, -1.972586f),
		vec3(-4.446338f, -6.479880f, -1.908469f),
		vec3(-4.548537f, -6.379836f, -2.008601f),

		vec3(0.0f, 0.0f, 0.0f),

		vec2(0.0f),
		vec2(0.0f),
		vec2(0.0f)
	};

	Image image;
	image_load_PPM("texture.ppm", image);

	vec3 plane_normal = triangleNormal(triangle.a, triangle.b, triangle.c);
	vec3 plane_intersection_point = vec3(-0.090023f, 7.007994f, -6.319276f);

	float da = point_to_edge_distance(triangle.a, triangle.b, plane_normal, plane_intersection_point);
	float db = point_to_edge_distance(triangle.b, triangle.c, plane_normal, plane_intersection_point);
	float dc = point_to_edge_distance(triangle.c, triangle.a, plane_normal, plane_intersection_point);

	if((da >= 0 && db >= 0 && dc >= 0) || (da <= 0 && db <= 0 && dc <= 0)) {
		printf("Edge distances E_a %f, E_b %f, E_c %f\n", da, db, dc);
		vec2 uv = uv_at_triangle_intersection_point(triangle, image, plane_intersection_point);
		vec3 color = image_read_color(image, uv);
	}

	return true;
}
#endif

#define TEST(x, y) \
	printf("> %s\n", y); \
	if(!x()) { printf("> FAILED\n"); return false; }\
	else {printf("> PASSED\n"); }

bool test()
{
	TEST(test_image, "Testing various image functions");
	TEST(test_plane_intersection, "Testing various ray/plane intersection scenarios");
	TEST(test_ray_plane_intersection, "Test a corner-case ray/plane intersection");
	//TEST(test_point_to_edge_distance, "Testing if the point distance to triangle edge calculation works as expected");
	return true;
}
