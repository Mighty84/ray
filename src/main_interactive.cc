/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include "common.h"
#include "scene.h"
#include "image.h"
#include "gbuffer.h"
#include "raytracer.h"
#include "rasterizer.h"
#include "postprocess.h"
#include "test.h"

#include <SDL.h>

void render_image(Scene& scene, mat4& viewMatrix, float fov, GBuffer& gbuffer, GBuffer& rbuffer)
{
	// Render scene to image buffer
	Timer raytrace_timer;
	timer_start(raytrace_timer);

	if(raytrace_scene_pass)
		raytracer_render_scene(scene, viewMatrix, fov, gbuffer);
	else
		rasterizer_render_scene(scene, viewMatrix, fov, gbuffer);

	float render_time = timer_stop(raytrace_timer);
	printf("Scene rendered in %fs ~ %ffps\n", render_time, 1.0f/render_time);
}

void render_raytrace(Scene& scene, GBuffer& gbuffer)
{
	// Save scene pass gbuffer
	gbuffer_save_PPM(gbuffer, "scene");

	// Render reflectance
	GBuffer rbuffer;
	gbuffer_init(rbuffer, gbuffer.width, gbuffer.height);

	Timer raytrace_reflectance_timer;
	timer_start(raytrace_reflectance_timer);

	raytracer_render_reflectance(scene, gbuffer, rbuffer);

	float render_reflectance_time = timer_stop(raytrace_reflectance_timer);
	printf("Reflectance rendered in %fs ~ %ffps\n", render_reflectance_time, 1.0f/render_reflectance_time);

	// Now apply FXAA
	if(postprocess_fxaa_enable) {
		Image rbuffer_color_image;
		image_init(rbuffer_color_image, gbuffer.width, gbuffer.height, 3);

		gbuffer_blit_color(rbuffer, rbuffer_color_image);
		image_save_PPM(rbuffer_color_image, "reflectance.ppm");

		Image fxaa_image;
		image_init(fxaa_image, gbuffer.width, gbuffer.height, 3);

		Timer fxaa_timer;
		timer_start(fxaa_timer);

		postprocess_fxaa(rbuffer_color_image, fxaa_image);

		printf("FXAA applied in %fs\n", timer_stop(fxaa_timer));

		if(postprocess_tonemapping_enable)
			postprocess_tonemap_buffer(fxaa_image, fxaa_image);

		image_save_PPM(fxaa_image, "fxaa.ppm");
		image_free(fxaa_image);

		image_free(rbuffer_color_image);
	}
	
	gbuffer_free(rbuffer);
}

int main()
{
	const uint32_t width = 1024;
	const uint32_t height = 768;

	if(SDL_Init(SDL_INIT_EVENTS|SDL_INIT_VIDEO) == -1)
		return 1;

	SDL_Window *screen = SDL_CreateWindow("Ray", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, 0);
	if(!screen) {
		fprintf(stderr, "Could not create window\n");
		return 1;
	}

	SDL_Renderer *renderer = SDL_CreateRenderer(screen, -1, SDL_RENDERER_ACCELERATED|SDL_RENDERER_PRESENTVSYNC);
	//SDL_Renderer *renderer = SDL_CreateRenderer(screen, -1, SDL_RENDERER_SOFTWARE);
	if(!renderer) {
		fprintf(stderr, "Could not create renderer\n");
		return 1;
	}

	SDL_SetRelativeMouseMode(SDL_TRUE);

	const float fov = 50.0f;
	vec3 camera_position = vec3(0.5f, 0.25f, -3.0f);
	vec3 camera_rotation = vec3(0.0f, 1.0f, 0.0f);
	mat4 viewMatrix = mat4(1.0f);

	// Create the scene
	Scene scene;
	scene_load(scene);

	// Allocate gbuffer
	GBuffer gbuffer;
	gbuffer_init(gbuffer, width, height);

	unordered_map<uint32_t, bool> keymap;
	bool quit = false;
	while(!quit) {

		SDL_Event event;
		while(SDL_PollEvent(&event)) {
			if(event.type == SDL_QUIT)
				quit = true;

			if(event.type == SDL_KEYUP)
				keymap[event.key.keysym.sym] = false;

			if(event.type == SDL_KEYDOWN)
				keymap[event.key.keysym.sym] = true;

			if(event.type == SDL_MOUSEMOTION) {
				int xrel = event.motion.xrel;
				if(xrel > 0) camera_rotation += vec3(0.0f, 0.02f, 0.0f);
				if(xrel < 0) camera_rotation += vec3(0.0f, -0.02f, 0.0f);

				int yrel = event.motion.yrel;
				if(yrel > 0) camera_rotation += vec3(-0.02f, 0.0f, 0.0f);
				if(yrel < 0) camera_rotation += vec3(0.02f, 0.0f, 0.0f);
			}
		}

		if(keymap[SDLK_ESCAPE])
			quit = true;

		if(keymap[SDLK_r])
			render_raytrace(scene, gbuffer);

		// Apply movement to axis in FPS fashion
		vec3 td = vec3(0.0f);

		if(keymap[SDLK_LEFT])
			td += vec3(-0.2f, 0.0f, 0.0f);

		if(keymap[SDLK_RIGHT])
			td += vec3(0.2f, 0.0f, 0.0f);

		if(keymap[SDLK_DOWN])
			td += vec3(0.0f, 0.0f, 0.2f);

		if(keymap[SDLK_UP])
			td += vec3(0.0f, 0.0f, -0.2f);

		if(keymap[SDLK_a])
			td += vec3(0.0f, 0.2f, 0.0f);

		if(keymap[SDLK_y])
			td += vec3(0.0f, -0.2f, 0.0f);

		float txr = -(cos(camera_rotation.y)*td.x - sin(camera_rotation.y)*td.z); 
		float tyr = -(sin(camera_rotation.y)*td.x + cos(camera_rotation.y)*td.z);
		camera_position += vec3(txr, td.y, tyr);

		// Calculate view-matrix from rotation and translation
		viewMatrix = mat4(1.0f);
		viewMatrix = rotate(viewMatrix, camera_rotation.x, vec3(1.0f, 0.0f, 0.0f));
		viewMatrix = rotate(viewMatrix, camera_rotation.y, vec3(0.0f, 1.0f, 0.0f));
		viewMatrix = rotate(viewMatrix, camera_rotation.z, vec3(0.0f, 0.0f, 1.0f));
		viewMatrix = translate(viewMatrix, camera_position);

		uint32_t fragment_buffer_size = width * height * sizeof(Fragment);
		uint32_t fragment_attribute_buffer_size = width * height * sizeof(FragmentAttributes);
		memset(gbuffer.fragments, 0x0, fragment_buffer_size);
		memset(gbuffer.fragment_attributes, 0x0, fragment_attribute_buffer_size);

		render_image(scene, viewMatrix, fov, gbuffer, gbuffer);

		SDL_Texture *texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGB24, SDL_TEXTUREACCESS_STREAMING, width, height);
		uint8_t *pixels = (uint8_t*) malloc(width * height * 3);
		for(uint32_t y = 0; y < height; y++)
			for(uint32_t x = 0; x < width; x++)
				for(uint32_t c = 0; c < 3; c++) {
					uint32_t offset = (y * width * 3) + (x * 3) + c;
					pixels[offset] = (uint8_t) (gbuffer.fragment_attributes[y*width + x].color[c] * 255.0f);
				}

		SDL_UpdateTexture(texture, NULL, pixels, width * 3);
		SDL_RenderClear(renderer);
		//SDL_SetRenderDrawColor(renderer, 255, 0, 0, 255);
		//SDL_RenderFillRect(renderer, NULL);
		SDL_RenderCopy(renderer, texture, NULL, NULL);
		SDL_RenderPresent(renderer);
		free(pixels);
		SDL_DestroyTexture(texture);
	}

	gbuffer_free(gbuffer);

	// Remove scene and associated assets
	scene_free(scene);

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(screen);
	SDL_Quit();

	return EXIT_SUCCESS;
}
