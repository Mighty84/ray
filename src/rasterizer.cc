/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include "common.h"
#include "scene.h"
#include "geometry.h"
#include "material.h"
#include "image.h"
#include "triangle.h"
#include "cluster.h"
#include "gbuffer.h"

#include "ray.h"

#define DEBUG_STATISTICS 0
#define DEBUG 0

#if DEBUG_STATISTICS
static uint32_t stat_visible_triangles = 0;
#endif

static vec4 near_plane_intersection(const vec4& x1, const vec4& x2)
{
	static const Plane plane = { vec3(0.0f, 0.0f, -projection_near), vec3(0.0f, 0.0f, -1.0f) };

	vec4 ray_direction = normalize(x1 - x2);
	float distance = -(dot(vec3(x1) - plane.origin, plane.normal))/dot(plane.normal, vec3(ray_direction));
	vec4 xr = x1 + (ray_direction * distance);

	return xr;
}

struct Extents {
	float xmin;
	float xmax;
	float ymin;
	float ymax;
};

static bool calculate_extents(vec4 cs_a, vec4 cs_b, vec4 cs_c, uint32_t width, uint32_t height, Extents& extents)
{
	// Is any vertex clipped
	bool clip_a = cs_a.z < -projection_near;
	bool clip_b = cs_b.z < -projection_near;
	bool clip_c = cs_c.z < -projection_near;
	const uint8_t clips = clip_a + clip_b + clip_c;

	// All three vertices are behind the near plane
	if(clips == 3)
		return false;

	// Clip for two vertices behind the near plane
	if(clips == 2) {
		if(clip_a && !clip_b) cs_a = near_plane_intersection(cs_a, cs_b);
		if(clip_a && !clip_c) cs_a = near_plane_intersection(cs_a, cs_c);

		if(clip_b && !clip_a) cs_b = near_plane_intersection(cs_b, cs_a);
		if(clip_b && !clip_c) cs_b = near_plane_intersection(cs_b, cs_c);

		if(clip_c && !clip_a) cs_c = near_plane_intersection(cs_c, cs_a);
		if(clip_c && !clip_b) cs_c = near_plane_intersection(cs_c, cs_b);
	}

	// Clip for one polygon behind the near plane
	bool split = false;
	vec4 cs_d;
	if(clips == 1) {
		if(clip_a) {
			cs_a = near_plane_intersection(cs_a, cs_b);
			cs_d = near_plane_intersection(cs_a, cs_c);
			split = true;
		}

		if(clip_b) {
			cs_b = near_plane_intersection(cs_b, cs_a);
			cs_d = near_plane_intersection(cs_b, cs_c);
			split = true;
		}

		if(clip_c) {
			cs_c = near_plane_intersection(cs_c, cs_a);
			cs_d = near_plane_intersection(cs_c, cs_b);
			split = true;
		}
	}

	// Clip-space to NDC
	// Apply perspective divide
	vec2 ndc_a = cs_a/cs_a.w;
	vec2 ndc_b = cs_b/cs_b.w;
	vec2 ndc_c = cs_c/cs_c.w;

	// Find the extents of the triangle in normal-space
	vec2 ndc_min = glm::min(ndc_a, glm::min(ndc_b, ndc_c));
	vec2 ndc_max = glm::max(ndc_a, glm::max(ndc_b, ndc_c));

	if(split) {
		vec2 ndc_d = cs_d/cs_d.w;

		ndc_min = glm::min(ndc_min, ndc_d);
		ndc_max = glm::max(ndc_max, ndc_d);
	}

	// Convert extents to screen_space
	const vec2 ss_min = (0.5f*(ndc_min+1.0f))*vec2(width, height);
	const vec2 ss_max = (0.5f*(ndc_max+1.0f))*vec2(width, height);

//	if(clips)
//		printf("Clips: %u Extents min: %s max: %s\n", clips, to_string(ss_min).c_str(), to_string(ss_max).c_str());

	// Discard fragments outside of screen-space
	extents.xmin = glm::clamp(ss_min.x, 0.0f, width-1.0f);
	extents.xmax = glm::clamp(ss_max.x, 0.0f, width-1.0f);
	extents.ymin = glm::clamp(ss_min.y, 0.0f, height-1.0f);
	extents.ymax = glm::clamp(ss_max.y, 0.0f, height-1.0f);

//	if(clips)
//		printf("Clips: %u x: %f %f, y: %f %f\n", clips, extents.xmin, extents.xmax, extents.ymin, extents.ymax);

	return true;
}

static void draw_triangle(const Object* object, const Triangle& triangle, const vec3& camera_position, const mat4& pvMatrix, GBuffer& gbuffer)
{
	// Project to view-space and then clip-space in one operation
	const vec4 cs_a = pvMatrix * vec4(triangle.a, 1.0f);
	const vec4 cs_b = pvMatrix * vec4(triangle.b, 1.0f);
	const vec4 cs_c = pvMatrix * vec4(triangle.c, 1.0f);

	Extents extents;
	if(!calculate_extents(cs_a, cs_b, cs_c, gbuffer.width, gbuffer.height, extents))
		return;

	// Prepare the inverse projection towards barycentric coordinates
	const mat3 inv_barycentric_matrix = inverse(mat3(
		cs_a.x, cs_a.y, cs_a.w,
		cs_b.x, cs_b.y, cs_b.w,
		cs_c.x, cs_c.y, cs_c.w
	));

	// Raster the triangle, i.e. go through every pixel, find the corresponding
	// barycentric coordinates (weights) so we can interpolate vertex attributes such as depth, uv, ...
	// Then perform a lookup and store the result to the output images
	for(uint16_t y = extents.ymin; y <= extents.ymax; y++) {

		const float cs_y = 2.0f * ((y / (float) gbuffer.height) - 0.5f);
		vec3 e2 = inv_barycentric_matrix[2];
		vec3 e1 = inv_barycentric_matrix[1] * cs_y + e2;

		for(uint16_t x = extents.xmin; x <= extents.xmax; x++) {

			const float cs_x = 2.0f * ((x / (float) gbuffer.width) - 0.5f);

			// We split the matrix multiplication on the axis basically doing this:
			//vec3 weights = inv_barycentric_matrix * vec3(cs_x, cs_y, 1.0f);
			vec3 e0 = inv_barycentric_matrix[0] * cs_x;
			vec3 weights = e0 + e1;

			if(weights.x < 0.0f || weights.y < 0.0f || weights.z < 0.0f)
				continue;

			weights = weights/(weights.x+weights.y+weights.z);
			vec3 intersection_point = triangle_interpolate<vec3>(triangle.a, triangle.b, triangle.c, weights);

			// Depth test
			Fragment screen_fragment;
			gbuffer_read_fragment(gbuffer, x, y, &screen_fragment);

			if(screen_fragment.object != NULL) {
				float screen_distance = length(screen_fragment.intersection_point - screen_fragment.source);
				float distance = length(intersection_point - camera_position);

				if(screen_distance < distance)
					continue;
			}

			// Only store sampled data after successful depth test
			Fragment fragment = {
				object,
				&triangle,
				intersection_point,
				camera_position,
				weights
			};

			gbuffer_write_fragment(gbuffer, fragment, x, y);
		}
	}
}

void rasterizer_render_scene(const Scene& scene, const mat4& viewMatrix, float fov, GBuffer& gbuffer)
{
	const mat4 projectionMatrix = perspective(radians(fov), gbuffer.width /(float) gbuffer.height, projection_near, projection_far);
	const mat4 pvMatrix = projectionMatrix * viewMatrix; // Precalculate

	const vec3 camera_position = inverse(viewMatrix)[3];
	const vec3 camera_direction = inverse(viewMatrix) * vec4(0.0f, 0.0f, -1.0f, 0.0f);

	// Add some tolerance to the angle when faces are facing away from
	// camera to compensate perspective expansion. This can probably be calculated.
	const float tolerance = 0.3f;

#if DEBUG
	printf("Size of triangle: %lu\n", sizeof(Triangle));
	printf("Size of fragment: %lu\n", sizeof(Fragment));
	printf("Size of cache-line: %lu\n", sysconf(_SC_LEVEL1_DCACHE_LINESIZE));
#endif

	vector<const Cluster *> clusters;
	clusters.reserve(64);

	// TODO factor in model-matrix
	for(const Object* object : scene.objects) {

		if(!cluster_frustum_intersection(object->cluster, camera_position, camera_direction, clusters))
			continue;

		for(auto cluster : clusters)
			for(uint32_t fi = 0; fi < cluster->faces.size(); fi++) {
				const Triangle& triangle = cluster->faces[fi];

				// Triangle is facing away from camera
				// Don't do anything
				const float cm_to_tr = dot(triangle.flat_normal, camera_direction);
				if(cm_to_tr >= 0.0f + tolerance)
					continue;

#if DEBUG_STATISTICS
				++stat_visible_triangles;
#endif

				draw_triangle(object, triangle, camera_position, pvMatrix, gbuffer);
			}

		clusters.clear();
	}

#if DEBUG_STATISTICS
	printf("Visible triangles: %u\n", stat_visible_triangles);
#endif

	const uint16_t width = gbuffer.width;
	const uint16_t height = gbuffer.height;

	#pragma omp parallel num_threads(raytrace_threads)
	#pragma omp for
	for(uint16_t y = 0; y < height; y++)
		for(uint16_t x = 0; x < width; x++) {
			Fragment fragment;
			gbuffer_read_fragment(gbuffer, x, y, &fragment);

			if(fragment.object == NULL)
				continue;

			FragmentAttributes fragment_attributes;
			material_compute_fragment_attributes(fragment, fragment_attributes);
			gbuffer_write_fragment_attributes(gbuffer, fragment_attributes, x, y);
		}
}
