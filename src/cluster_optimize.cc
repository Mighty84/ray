/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include "common.h"
#include "geometry.h"

#include <stdexcept>

static void find_unlocked_vertices(const Cluster& c,
	unordered_map<vec3, unordered_map<vec3, vector<uint32_t>>>& unlocked_edges)
{
	// Index a map of all edges
	unordered_map<vec3, unordered_map<vec3, vector<uint32_t>>> edge_map;

	for(uint32_t fi = 0; fi < c.faces.size(); fi++) {
		const Triangle& t = c.faces[fi];

		edge_map[t.a][t.b].push_back(fi);
		edge_map[t.b][t.c].push_back(fi);
		edge_map[t.c][t.a].push_back(fi);
	}

	// Iterate through all map elements and find entries only used once.
	// These are edges and the vertices should be locked.
	unordered_map<vec3, vector<uint32_t>> locked_vertices;

	for(auto &x : edge_map)
		for(auto &y : x.second) {

			uint32_t ab_size = y.second.size();
			uint32_t ba_size = 0;
			try {
				ba_size = edge_map.at(y.first).at(x.first).size();
			} catch(const std::out_of_range& e) {}

			if(ab_size + ba_size == 1) {
				// Locked edges (ie. the vertices used) shall not be optimized
				locked_vertices[x.first].push_back(y.second[0]);
				locked_vertices[y.first].push_back(y.second[0]);
			}
		}

	// Unlocked edges shall contain only edges where both
	// vertices are unlocked (for now)
	for(auto &x : edge_map) {
		if(locked_vertices.find(x.first) != locked_vertices.end())
			continue;

		for(auto &y : x.second) {
			if(locked_vertices.find(y.first) != locked_vertices.end())
				continue;

			unlocked_edges[x.first][y.first].insert(
					unlocked_edges[x.first][y.first].end(),
					y.second.begin(), y.second.end()
				);

			unlocked_edges[y.first][x.first].insert(
					unlocked_edges[y.first][x.first].end(),
					y.second.begin(), y.second.end()
				);
		}
	}

	printf("Locked/unlocked vertices %lu/%lu\n", locked_vertices.size(), unlocked_edges.size());
}

static void collapse_cluster_triangle(Cluster& c, uint32_t fi)
{
	Triangle *fr = &c.faces[fi];

	vec3 v_new = (fr->a + fr->b + fr->c) / 3.0f;
	vec3 vn_new = (fr->vertex_normal1 + fr->vertex_normal2 + fr->vertex_normal3) / 3.0f;
	vec2 uv_new = (fr->uv1 + fr->uv2) / 2.0f;

	// TODO So far no need reasoned why the tangent normal needs to be recalculated
	vec3 tn_new = (fr->tangent_normal1 + fr->tangent_normal2 + fr->tangent_normal3) / 3.0f;

	for(uint32_t ffi = 0; ffi < c.faces.size(); ffi++) {
		if(ffi == fi)
			continue;

		Triangle& f = c.faces[ffi];

		if(f.a == fr->a) { f.a = v_new; f.vertex_normal1 = vn_new; f.uv1 = uv_new; }
		if(f.b == fr->a) { f.b = v_new; f.vertex_normal2 = vn_new; f.uv2 = uv_new; }
		if(f.c == fr->a) { f.c = v_new; f.vertex_normal3 = vn_new; f.uv3 = uv_new; }

		if(f.a == fr->b) { f.a = v_new; f.vertex_normal1 = vn_new; f.uv1 = uv_new; }
		if(f.b == fr->b) { f.b = v_new; f.vertex_normal2 = vn_new; f.uv2 = uv_new; }
		if(f.c == fr->b) { f.c = v_new; f.vertex_normal3 = vn_new; f.uv3 = uv_new; }

		if(f.a == fr->c) { f.a = v_new; f.vertex_normal1 = vn_new; f.uv1 = uv_new; }
		if(f.b == fr->c) { f.b = v_new; f.vertex_normal2 = vn_new; f.uv2 = uv_new; }
		if(f.c == fr->c) { f.c = v_new; f.vertex_normal3 = vn_new; f.uv3 = uv_new; }

		f.flat_normal = triangleNormal(f.c, f.b, f.a);
	}
}

void optimize_cluster_mesh(Cluster& c)
{
	printf("Optimizing cluster with %lu faces\n", c.faces.size());

	unordered_map<vec3, unordered_map<vec3, vector<uint32_t>>> unlocked_edges;
	find_unlocked_vertices(c, unlocked_edges);

	// Calculate the average length of all edges in this cluster
	float avg_distance = 0.0f;
	float avg_elements = 0.0f;

	for(auto &x : unlocked_edges)
		for(auto &y : x.second) {
			avg_distance += length(x.first - y.first);
			++avg_elements;
		}

	avg_distance /= avg_elements;

	// Collapse faces that are fully unlocked
	for(uint32_t fi = 0; fi < c.faces.size();) {
		Triangle &f = c.faces[fi];
		bool unlocked = false;

		try {
			unlocked = unlocked_edges.at(f.a).at(f.b).size() > 0 &&
				unlocked_edges.at(f.b).at(f.c).size() > 0 &&
				unlocked_edges.at(f.c).at(f.a).size() > 0;
		} catch(const std::out_of_range& e) {}

		bool small = false;

		if(unlocked) {
			try {
				small = length(f.a - f.b) < avg_distance ||
					length(f.b - f.c) < avg_distance ||
					length(f.c - f.a) < avg_distance;
			} catch(const std::out_of_range& e) {}
		}

		if(unlocked && small) {
			collapse_cluster_triangle(c, fi);
			c.faces.erase(c.faces.begin() + fi);
		} else
			++fi;
	}

	// Remove faces where edges collapsed
	for(uint32_t fi = 0; fi < c.faces.size();) {
		Triangle *tr = &c.faces[fi];

		if(tr->a == tr->b || tr->a == tr->c || tr->b == tr->c)
			c.faces.erase(c.faces.begin() + fi);
		else
			++fi;
	}

	c.faces.shrink_to_fit();

	printf("Cluster was optimized to %lu faces\n", c.faces.size());
}
