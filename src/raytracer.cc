/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include "common.h"
#include "ray.h"
#include "triangle.h"
#include "cluster.h"
#include "material.h"
#include "image.h"
#include "gbuffer.h"

#define DEBUG_STATISTICS 0
#define DEBUG 0

#if DEBUG_STATISTICS
static uint32_t stat_rays_traced = 0;
#endif

static bool trace_ray(const Ray& ray, const Scene& scene, int32_t x, int32_t y, Fragment& fragment)
{
#if DEBUG_STATISTICS
	++stat_rays_traced;
#endif

	Triangle *sample_triangle;
	vec3 sample_intersection_point;
	float sample_distance = projection_far;

	// Go through all intersected objects and find (the closest) hit
	for(const Object* object : scene.objects)
		if(cluster_ray_intersection(object->cluster, ray, &sample_triangle, &sample_intersection_point, &sample_distance)) {
			fragment.object = object;
			fragment.triangle = sample_triangle;
			fragment.intersection_point = sample_intersection_point;
			fragment.source = ray.origin;
		}

	if(fragment.object == NULL)
		return false;

	// Otherwise finalize the fragment processing by calculating the barycentric weights
	fragment.weights = triangle_barycentric_weights<vec3>(sample_triangle->a, sample_triangle->b,
		sample_triangle->c, sample_intersection_point);

	return true;
}

static void bounce_ray(const Scene& scene, Fragment& fragment, FragmentAttributes& fragment_attributes,
	uint32_t x, uint32_t y, uint8_t step)
{
	// Return immediately if we've surpassed the max. number of bounces
	if(step >= reflectance_max_steps)
		return;

	// The ray did not hit anything we can bounce off of
	if(fragment.object == NULL)
		return;

	// Compute the rays we will be using in the current step
	Ray sample_rays[reflectance_max_samples];
	uint8_t sample_ray_count = material_compute_sample_rays(fragment, fragment_attributes, sample_rays, step);

	// Prepare storage for results
	Fragment reflectance_samples[reflectance_max_samples];
	memset(reflectance_samples, 0x0, sizeof(reflectance_samples));

	FragmentAttributes reflectance_sample_attributes[reflectance_max_samples];
	memset(reflectance_sample_attributes, 0x0, sizeof(reflectance_sample_attributes));

	// Trace from bounce to next fragment
	for(uint8_t si = 0; si < sample_ray_count; si++) {
		if(trace_ray(sample_rays[si], scene, x, y, reflectance_samples[si])) {
			material_compute_fragment_attributes(reflectance_samples[si], reflectance_sample_attributes[si]);
			bounce_ray(scene, reflectance_samples[si], reflectance_sample_attributes[si], x, y, step+1);
		}
	}

	// Aggregate the fragment-attributes
	material_compute_sample_aggregation(reflectance_sample_attributes, fragment_attributes);
}

void raytracer_render_reflectance(const Scene& scene, const GBuffer& input_gbuffer, GBuffer& output_gbuffer)
{
	// For every pixel, grab the gbuffer fragment and interate it reflectance_max_steps times
	#pragma omp parallel num_threads(raytrace_threads)
	#pragma omp for
	for(uint32_t y = 0; y < input_gbuffer.height; y++)
		for(uint32_t x = 0; x < input_gbuffer.width; x++) {

			Fragment screen_fragment;
			FragmentAttributes screen_fragment_attributes;
			gbuffer_read_fragment(input_gbuffer, x, y, &screen_fragment);
			gbuffer_read_fragment_attributes(input_gbuffer, x, y, &screen_fragment_attributes);

			bounce_ray(scene, screen_fragment, screen_fragment_attributes, x, y, 0);

			gbuffer_write_fragment(output_gbuffer, screen_fragment, x, y);
			gbuffer_write_fragment_attributes(output_gbuffer, screen_fragment_attributes, x, y);
		}
}

void raytracer_render_scene(const Scene& scene, const mat4& viewMatrix, float fov, GBuffer& gbuffer)
{
#if DEBUG_STATISTICS
	Timer raycount_timer;
	timer_start(raycount_timer);
	stat_rays_traced = 0;
#endif

	const float fov_f = tan(radians(fov/2.0f));
	const float aspect = gbuffer.width / (float) gbuffer.height;

	vec3 camera_position = inverse(viewMatrix)[3];
	mat3 camera_direction_matrix = inverse(viewMatrix);

	#pragma omp parallel num_threads(raytrace_threads)
	#pragma omp for
	for(uint32_t y = 0; y < gbuffer.height; y++) {

		// Convert screen location to NDC
		const float ndc_yf = 2.0f * ((y / (float) gbuffer.height) - 0.5f) * fov_f;

		for(uint32_t x = 0; x < gbuffer.width; x++) {

			// Convert screen location to NDC (actually camera-space, as we also adjust for fov and aspect)
			const float ndc_xf = 2.0f * ((x / (float) gbuffer.width) - 0.5f) * fov_f * aspect;

			// Cast ray down the z axis
			vec3 cast_direction = camera_direction_matrix * vec3(ndc_xf, ndc_yf, -1.0f);

			// Ray direction should be normalized to avoid warping (scaling) the axis later
			cast_direction = normalize(cast_direction);

			Ray ray = {
				camera_position,
				cast_direction
			};

			// Trace the ray and store to gbuffer
			Fragment fragment;
			memset(&fragment, 0x0, sizeof(fragment));

			if(trace_ray(ray, scene, x, y, fragment)) {
				gbuffer_write_fragment(gbuffer, fragment, x, y);

				FragmentAttributes fragment_attributes;
				material_compute_fragment_attributes(fragment, fragment_attributes);
				gbuffer_write_fragment_attributes(gbuffer, fragment_attributes, x, y);
			}
		}
	}

#if DEBUG_STATISTICS
	float t = timer_stop(raycount_timer);
	printf("%u rays traced in %fs ~ %.2f rays/s\n", stat_rays_traced, t, stat_rays_traced/t);
#endif
}
