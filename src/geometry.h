/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef GEOMETRY_H
#define GEOMETRY_H

bool geometry_load(Geometry& geometry, const char *filename);
void geometry_free(Geometry& geometry);

inline uint32_t geometry_get_face_count(const Geometry& geometry)
{
	return geometry.face_count;
}

inline void geometry_fetch_triangle(const Geometry& geometry, uint32_t face, Triangle *triangle)
{
	float *v = &geometry.vertices[geometry.frame_num][face*3*3];
	float *u = &geometry.texture_coordinates[face*3*2];
	float *vn = &geometry.normals[geometry.frame_num][face*3*3];
	vec3 *tn = &geometry.tangent_normals[face*3];

	*triangle = {
		vec3(v[0], v[1], v[2]),
		vec3(v[3], v[4], v[5]),
		vec3(v[6], v[7], v[8]),

		geometry.flat_normals[face],

		vec3(vn[0], vn[1], vn[2]),
		vec3(vn[3], vn[4], vn[5]),
		vec3(vn[6], vn[7], vn[8]),

		vec2(u[0], u[1]),
		vec2(u[2], u[3]),
		vec2(u[4], u[5]),

		tn[0],
		tn[1],
		tn[2]
	};
}

#endif
