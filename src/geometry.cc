/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include "common.h"

#define DEBUG 0

// For intersection tests, we need the precise plane normal
// The vertex-normals supplied by Blender are for lighting-calculations only
// TODO this should be done for every geometry.frame_num
static void precalculate_flat_normals(Geometry& geometry)
{
	geometry.flat_normals = (vec3*) malloc(geometry.face_count * sizeof(vec3));

	for(uint32_t f = 0; f < geometry.face_count; f++) {

		float *v = &geometry.vertices[0][f*3*3];

		vec3 t_a = vec3(v[0], v[1], v[2]);
		vec3 t_b = vec3(v[3], v[4], v[5]);
		vec3 t_c = vec3(v[6], v[7], v[8]);

		// We expect CCW for correct orientation
		geometry.flat_normals[f] = triangleNormal(t_c, t_b, t_a);
	}
}

static void precalculate_tangent_normals(Geometry& geometry)
{
	uint32_t vertex_count = geometry.face_count * 3;
	geometry.tangent_normals = (vec3*) malloc(vertex_count * sizeof(vec3));

	for(uint32_t f = 0; f < geometry.face_count; f++) {

		// Get face vertices
		float *v = &geometry.vertices[0][f*3*3];
		vec3 t_a = vec3(v[0], v[1], v[2]);
		vec3 t_b = vec3(v[3], v[4], v[5]);
		vec3 t_c = vec3(v[6], v[7], v[8]);

		// Get scale from t2 to t0 on y axis
		// Get scale from t1 to t0 on y axis
		float *uv = &geometry.texture_coordinates[f*6];
		float t2t0 = uv[4+1] - uv[1];
		float t1t0 = uv[2+1] - uv[1];

		vec3 tan = (t_b-t_a)*t2t0 - (t_c-t_a)*t1t0;
		tan = normalize(tan);
		//printf("Resulting tangent is %s\n", to_string(tan).c_str());

		// Save tangent normal
		vec3 *tn = &geometry.tangent_normals[f*3];
		for(uint32_t vi = 0; vi < 3; vi++)
			tn[vi] = vec3(tan.x, tan.y, tan.z);
	}
}

static void verify_uv(const Geometry& geometry)
{
	for(uint32_t f=0; f < geometry.face_count; f++) {
		float *uv = &geometry.texture_coordinates[f*6];
		for(uint8_t i = 0; i < 6; i++)
			assert(uv[i] >= 0.0f && uv[i] <= 1.0f);
	}
}

bool geometry_load(Geometry& geometry, const char *filename)
{
	printf("Loading geometry file: %s\n", filename);

	// Open file
	FILE *gf = fopen(filename, "rb");

	if(gf == NULL) {
		printf("File not found: %s", filename);
		return false;
	}

	// Read frame and vertex count
	fread(&geometry.frame_count, sizeof(uint32_t), 1, gf);
	fread(&geometry.face_count, sizeof(uint32_t), 1, gf);

	printf("Geometry file has %u frame(s) with %u faces\n", geometry.frame_count, geometry.face_count);

	// Read vertices and normals for each frame
	geometry.vertices = (float**) malloc(geometry.frame_count * sizeof(float*));
	geometry.normals = (float**) malloc(geometry.frame_count * sizeof(float*));

	uint32_t vertex_count = geometry.face_count * 3;
	uint32_t float_count = vertex_count * 3;

	for(uint32_t f = 0; f < geometry.frame_count; f++) {
		geometry.vertices[f] = (float*) malloc(float_count * sizeof(float));
		geometry.normals[f] = (float*) malloc(float_count * sizeof(float));

		size_t res = 0;
		const size_t exp  = float_count * sizeof(float);

		res = fread(geometry.vertices[f], 1, exp, gf);
		if (res != exp)
			printf("Insufficient bytes read. Only %lu from %lu read\n", res, exp);

		res = fread(geometry.normals[f], 1, exp, gf);
		if (res != exp)
			printf("Insufficient bytes read. Only %lu from %lu read\n", res, exp);
	}

	// Read texture coordinates
	geometry.texture_coordinates = (float*) malloc(vertex_count * 2 * sizeof(float));

	size_t exp = vertex_count * 2 * sizeof(float);
	size_t res = fread(geometry.texture_coordinates, 1, exp, gf);
	if (res != exp)
		printf("Insufficient bytes read. Only %lu from %lu read\n", res, exp);

	// Close the file
	fclose(gf);

	// Animation starting frame
	geometry.frame_num = 0;

	// Verification of import
	verify_uv(geometry);

	// Precalculations
	precalculate_flat_normals(geometry);
	precalculate_tangent_normals(geometry);

	return true;
}

void geometry_free(Geometry& geometry)
{
	for(uint32_t f = 0; f < geometry.frame_count; f++) {
			free(geometry.vertices[f]);
			free(geometry.normals[f]);
	}

	free(geometry.vertices);
	free(geometry.normals);
	free(geometry.texture_coordinates);

	free(geometry.flat_normals);
	free(geometry.tangent_normals);
}
