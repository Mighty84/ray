/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include "common.h"

#include <openexr.h>
#include <ImfHeader.h>
#include <ImfChannelList.h>
#include <ImfOutputFile.h>
#include <ImfFrameBuffer.h>

static float *imagebuffer_allocate(uint32_t width, uint32_t height, uint8_t channels)
{
	uint32_t image_buffer_size = width * height * channels * sizeof(float);
	float *image_buffer = (float*) malloc(image_buffer_size);
	if(image_buffer == NULL) {
		fprintf(stderr, "Failed to allocate memory for image buffer.\n");
		exit(EXIT_FAILURE);
	}

	memset(image_buffer, 0x00, image_buffer_size);
	return image_buffer;
}

void image_save_EXR(const Image& image, const char *filename)
{
	assert(image.channels == 3);

	// Based on https://openexr.readthedocs.io/en/latest/ReadingAndWritingImageFiles.html
	Imf::Header header(image.width, image.height);
	header.channels().insert("R", Imf::Channel(Imf::FLOAT));
	header.channels().insert("G", Imf::Channel(Imf::FLOAT));
	header.channels().insert("B", Imf::Channel(Imf::FLOAT));

	Imf::OutputFile file(filename, header);
	Imf::FrameBuffer frameBuffer;

	frameBuffer.insert("R",
		Imf::Slice(Imf::FLOAT,
			(char *) image.buffer,
			sizeof(*image.buffer) * 1 * image.channels,
			sizeof(*image.buffer) * image.width * image.channels));

	frameBuffer.insert("G",
		Imf::Slice(Imf::FLOAT,
			(char *) image.buffer + sizeof(float) * 1,
			sizeof(*image.buffer) * 1 * image.channels,
			sizeof(*image.buffer) * image.width * image.channels));

	frameBuffer.insert("B",
		Imf::Slice(Imf::FLOAT,
			(char *) image.buffer + sizeof(float) * 2,
			sizeof(*image.buffer) * 1 * image.channels,
			sizeof(*image.buffer) * image.width * image.channels));

	file.setFrameBuffer(frameBuffer);
	file.writePixels(image.height);
}

void image_save_PPM(const Image& image, const char *filename)
{
	assert(image.channels == 3);

	printf("Saving image buffer to PPM with dimensions %ux%u and %u channels to \"%s\"\n",
		image.width, image.height, image.channels, filename);

	FILE *f = fopen(filename, "wb+");
	fprintf(f, "P6\n%d %d\n%d\n", image.width, image.height, 255);

	for(uint32_t y = 0; y < image.height; y++)
		for(uint32_t x = 0; x < image.width; x++)
			for(uint8_t c = 0; c < image.channels; c++) {

				uint32_t offset = (y * image.width * image.channels) + (x * image.channels) + c;
				uint8_t color = (uint8_t) (image.buffer[offset] * 255.0f); // Imprecise but who cares

				fwrite((void*) &color, sizeof(uint8_t), 1, f);
			}

	fclose(f);
}

bool image_load_PPM(const char *filename, Image& image)
{
	printf("Loading image from PPM: %s\n", filename);

	FILE *f = fopen(filename, "rb");
	if(f == NULL) {
		printf("File not found: %s\n", filename);
		return false;
	}

	// Read magic identifier
	uint32_t colors;
	int result1 = fscanf(f, "P6\n");
	if(result1 != 0) {
		printf("Invalid file header detected in %s\n", filename);
		fclose(f);
		return false;
	}

	// Skip comments
	long repos = ftell(f);
	if(fgetc(f) == '#')
		while(fgetc(f) != '\n');
	else
		fseek(f, repos, SEEK_SET);

	// Read image dimensions
	int result2 = fscanf(f, "%u %u\n%u\n", &image.width, &image.height, &colors);
	if(result2 != 3) {
		printf("Cannot read image dimensions from %s\n", filename);
		fclose(f);
		return false;
	}

	// Always assume this
	image.channels = 3;

	printf("Detected dimensions %ux%u with %u colors\n", image.width, image.height, colors);

	// Allocate appropriately sized image buffer
	image.buffer = imagebuffer_allocate(image.width, image.height, image.channels);

	for(uint32_t y = 0; y < image.height; y++)
		for(uint32_t x = 0; x < image.width; x++)
			for(uint8_t c = 0; c < image.channels; c++) {

				uint32_t offset = (y * image.width * image.channels) + (x * image.channels) + c;
				image.buffer[offset] = fgetc(f) / 255.0f; // TODO handle potential EOF error
			}

	fclose(f);
	return true;
}

void image_init(Image& image, uint32_t width, uint32_t height, uint8_t channels)
{
	image.width = width;
	image.height = height;
	image.channels = channels;
	image.buffer = imagebuffer_allocate(width, height, channels);
}

void image_free(Image& image)
{
	free(image.buffer);
}
