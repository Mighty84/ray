/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include "common.h"
#include "image.h"
#include "material.h"
#include "postprocess.h"
#include "gbuffer.h"

void gbuffer_init(GBuffer& gbuffer, uint32_t width, uint32_t height)
{
	gbuffer.width = width;
	gbuffer.height = height;

	// Allocate a fragment per-pixel
	uint32_t fragment_buffer_size = width * height * sizeof(Fragment);
	gbuffer.fragments = (Fragment*) malloc(fragment_buffer_size);
	if(gbuffer.fragments == NULL) {
		fprintf(stderr, "Failed to allocate memory for gbuffer fragments.\n");
		exit(EXIT_FAILURE);
	}

	memset(gbuffer.fragments, 0x0, fragment_buffer_size);

	// Allocate a fragment-attributes per-pixel
	uint32_t fragment_attribute_buffer_size = width * height * sizeof(FragmentAttributes);
	gbuffer.fragment_attributes = (FragmentAttributes*) malloc(fragment_attribute_buffer_size);
	if(gbuffer.fragment_attributes == NULL) {
		fprintf(stderr, "Failed to allocate memory for gbuffer fragment-attributes.\n");
		exit(EXIT_FAILURE);
	}

	memset(gbuffer.fragment_attributes, 0x0, fragment_attribute_buffer_size);
}

void gbuffer_free(GBuffer& gbuffer)
{
	free(gbuffer.fragments);
	free(gbuffer.fragment_attributes);
}

static void blit_all(const GBuffer& gbuffer, Image& depth, Image& wsc, Image& color, Image& normal)
{
	Timer blit_timer;
	timer_start(blit_timer);

	#pragma omp parallel num_threads(raytrace_threads)
	#pragma omp for
	for(uint32_t y = 0; y < gbuffer.height; y++)
		for(uint32_t x = 0; x < gbuffer.width; x++) {

			Fragment fragment;
			gbuffer_read_fragment(gbuffer, x, y, &fragment);

			if(fragment.object == NULL)
				continue;

			FragmentAttributes fragment_attributes;
			gbuffer_read_fragment_attributes(gbuffer, x, y, &fragment_attributes);

			// Store the results in the output buffers
			image_write_vec1(depth, x, y, vec1(length(fragment.intersection_point - fragment.source)));
			image_write_vec3(wsc, x, y, fragment.intersection_point);
			image_write_vec3(color, x, y, fragment_attributes.color);
			image_write_vec3(normal, x, y, fragment_attributes.normal);
		}

	printf("Images blit in %fs\n", timer_stop(blit_timer));
}

void gbuffer_blit_color(GBuffer& gbuffer, Image& output_image)
{
	Timer blit_timer;
	timer_start(blit_timer);

	#pragma omp parallel num_threads(raytrace_threads)
	#pragma omp for
	for(uint32_t y = 0; y < gbuffer.height; y++)
		for(uint32_t x = 0; x < gbuffer.width; x++) {

			FragmentAttributes fragment_attributes;
			gbuffer_read_fragment_attributes(gbuffer, x, y, &fragment_attributes);

			// Store the result in the output buffer
			image_write_vec3(output_image, x, y, fragment_attributes.color);
		}

	printf("Image blit in %fs\n", timer_stop(blit_timer));
}

void gbuffer_save_PPM(GBuffer& gbuffer, const char* filename_prefix)
{
	Image color;
	Image normal;
	Image wsc;
	Image depth;

	// Allocate color, normal and world-space coordinates buffer
	image_init(color, gbuffer.width, gbuffer.height, 3);
	image_init(normal, gbuffer.width, gbuffer.height, 3);
	image_init(wsc, gbuffer.width, gbuffer.height, 3);

	// Allocate and initialize depth buffer
	image_init(depth, gbuffer.width, gbuffer.height, 1);

	// TODO remove. Currently not used by rasterizers nor raytracers
	// depth test but for diagnostics only
	for(uint32_t y = 0; y < gbuffer.height; y++)
		for(uint32_t x = 0; x < gbuffer.width; x++)
			image_write_vec1(depth, x, y, vec1(projection_far));

	// Blit fragments/fragment-attributes to image buffers
	blit_all(gbuffer, depth, wsc, color, normal);

	// Prepend layer name
	char color_filename[PATH_MAX];
	char normal_filename[PATH_MAX];
	char wsc_filename[PATH_MAX];
	char depth_filename[PATH_MAX];
	sprintf(color_filename, "%s_color.ppm", filename_prefix);
	sprintf(normal_filename, "%s_normal.ppm", filename_prefix);
	sprintf(wsc_filename, "%s_wsc.ppm", filename_prefix);
	sprintf(depth_filename, "%s_depth.ppm", filename_prefix);

	// Save images
	image_save_PPM(color, color_filename);
	image_save_PPM(wsc, wsc_filename);

	// Scale and write normal buffer for visualization
	{
		Image scaled_normal_image;
		image_init(scaled_normal_image, normal.width, normal.height, 3);

		postprocess_normal_buffer(normal, scaled_normal_image);

		image_save_PPM(scaled_normal_image, normal_filename);
		image_free(scaled_normal_image);
	}

	// Remap and write depth buffer for visualization
	{
		// Linearized depth buffer has three channels
		Image linear_depth_image;
		image_init(linear_depth_image, depth.width, depth.height, 3);

		// Linearize and remap values
		postprocess_depth_buffer(depth, linear_depth_image);

		// Save as EXR
		char depth_filename_EXR[PATH_MAX];
		sprintf(depth_filename_EXR, "%s_depth.exr", filename_prefix);
		image_save_EXR(linear_depth_image, depth_filename_EXR);

		// Save as PPM
		image_save_PPM(linear_depth_image, depth_filename);

		image_free(linear_depth_image);
	}

	image_free(color);
	image_free(normal);
	image_free(wsc);
	image_free(depth);
}
