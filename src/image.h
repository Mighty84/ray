/*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*    http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#ifndef IMAGE_H
#define IMAGE_H

void image_save_EXR(const Image& image, const char *filename);
void image_save_PPM(const Image& image, const char *filename);
bool image_load_PPM(const char *filename, Image& image);
void image_init(Image& image, uint32_t width, uint32_t height, uint8_t channels);
void image_free(Image& image);

inline vec3 image_read_vec3(const Image& image, uint32_t x, uint32_t y)
{
	assert(image.channels == 3);
	assert(x < image.width);
	assert(y < image.height);

	uint32_t offset = (y * image.width * image.channels) + (x * image.channels);
	return vec3(image.buffer[offset+0], image.buffer[offset+1], image.buffer[offset+2]);
}

inline vec3 image_read_vec3(const Image& image, const vec2 uv)
{
	uint32_t x = (image.width-1) * uv.x;
	uint32_t y = (image.height-1) * uv.y;
	return image_read_vec3(image, x, y);
}

inline void image_write_vec3(Image& image, uint32_t x, uint32_t y, const vec3& v)
{
	assert(image.channels == 3);

	const uint32_t offset = (y*image.width*image.channels) + x*image.channels;
	image.buffer[offset+0] = v.x;
	image.buffer[offset+1] = v.y;
	image.buffer[offset+2] = v.z;
}

inline void image_write_vec3(Image& image, const vec2& uv, const vec3& v)
{
	uint32_t x = uv.x * image.width;
	uint32_t y = uv.y * image.height;
	image_write_vec3(image, x, y, v);
}

inline vec1 image_read_vec1(const Image& image, uint32_t x, uint32_t y)
{
	assert(image.channels == 1);

	uint32_t offset = y*image.width + x;
	return vec1(image.buffer[offset+0]);
}

inline void image_write_vec1(Image& image, uint32_t x, uint32_t y, const vec1& v)
{
	assert(image.channels == 1);

	const uint32_t offset = (y*image.width) + x;
	image.buffer[offset] = v.x;
}

#endif
