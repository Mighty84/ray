#!BPY
# """
# Name: 'Model Exporter (*.m)...'
# Group: 'Export'
# """
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os.path
import struct
import bpy
from mathutils import *

# Preset filename
filename = bpy.data.filepath.split(".blend")[0]+".m"

def write_frame(file, mesh, frame_num, object):
	
	for f in mesh.loop_triangles:
		for v in f.vertices:
			vv = mesh.vertices[v].co

			# Make the vertex coordinates world-coordinates
			vw = mesh.vertices[v].co @ object.matrix_world

			# Write to file whilst swapping Z (depth in GL) and Y (depth in Blender)
			file.write(struct.pack("<3f", -vw[0], -vw[2], vw[1]))

			#print("Wrote " + str(vw[0]) + ", " + str(vw[1]) + ", " + str(vw[2]))

	for f in mesh.loop_triangles:
		for v in f.vertices:
			vn = mesh.vertices[v].normal @ object.matrix_world

			# Write to file whilst swapping Z (depth in GL) and Y (depth in Blender)
			file.write(struct.pack("<3f", -vn[0], -vn[2], vn[1]))

def export(filename):
	global error

	# See if selection is mesh
	object = bpy.context.selected_objects[0]
	if object.type != "MESH":
		print("No mesh selected")
		return
	mesh = object.data
	
	mesh.calc_loop_triangles()

	# Count vertices without stride
	face_count = len(mesh.loop_triangles)

	# Number of frames, starting by zero, to export
	if(object.animation_data is not None):
		frames = int(object.animation_data.action.frame_range.y)
	else:
		frames = 1
		
	# Write data
	print("Exporting " + str(frames) + " frames with " + str(face_count) + " faces")

	file = open(filename, "wb")

	# Write number of frames and vertices
	# Framecount and number of vertices
	file.write(struct.pack("<2i", frames, face_count))

	# Write frame data
	for frame_num in range(0, frames):
		bpy.context.scene.frame_set(frame_num)
		write_frame(file, mesh, frame_num, object)

	# Write UV map
	uv_layer = mesh.uv_layers[0].data
	for f in mesh.loop_triangles:	
		for i in f.loops:
			(u, v) = uv_layer[i].uv
			file.write(struct.pack("<2f", u, 1-v))

	file.close()
	print("Done")

export(filename)
